/* ==== IMPORT PARAMS ==== */
'use strict';
import { lastRun } from 'gulp';
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inDev = 'development',
inDevApps = `${inDev}/components`;
// dir = `${__dirname}/`;
/* ==== ----- ==== */


const __cfg = {
	list: {
		css: [
		`${__dirname}/../node_modules/normalize.css/normalize.css`,
		`${__dirname}/../node_modules/owl.carousel/dist/assets/owl.carousel.min.css`,
		`${__dirname}/../node_modules/lite-padding-margin/dist/css-source/lite-padding-margin.min.css`
		],
		map: []
	}
};


/* ==== ----- ==== */

let sinceReplace = (x) => `${x}`.replace(/-/gi, ':');

module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	() => combiner(
	
			src([...__cfg.list.css, ...__cfg.list.map], { since: lastRun(sinceReplace(nameTask))} ),
			_run.newer(`${inDevApps}/plugins`),
			dest(`${inDevApps}/plugins`)
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));

  