module.exports = class AddPlayYoutube {
	constructor(item, number, drag) {
		this.item = item;
		this.number = number;
		this.drag = drag;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.item}`);
		if (elem) {
			this.constructor.info();
			
			const doit = (event) => {
				const qwe = $('#youtube-start-url').attr('data-lazy-src');
				$('#youtube-start-url').attr('src', qwe);
				$(`${this.item}`).fadeOut();
			};
			
			elem.addEventListener('click', event => doit(event));
		}
	}
};
