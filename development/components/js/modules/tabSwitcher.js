module.exports = class AddtabSwitcher {
	constructor(enter, exit) {
		this.enter = enter;
		this.exit = exit;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.enter}`);
		const output = document.querySelector(`${this.exit}`);
		if (elem) {
			this.constructor.info();

			const logic = (event) => {
				event.preventDefault();
				const curElem = event.target;
				const index = [...curElem.parentElement.children].indexOf(curElem);

				if (output.querySelector('.active')) {
					output.querySelector('.active').classList.remove('active');
				}
				output.children[index].classList.add('active');
			};

			output.firstElementChild.classList.add('active');
			elem.addEventListener('click', event => logic(event));
		}
	}
};
