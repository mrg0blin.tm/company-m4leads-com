module.exports = class AddFetchCarouselItems {
	constructor(enter, data, exit) {
		this.enter = enter;
		console.log('enter', enter);
		this.exit = exit;
		this.server = data.server;
		this.attr = data.selector;

		// this.myDiv = myDiv;
		// this.myClass = myClass;
		// this.myText = myText;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	createDiv(q, w, e, r) {
		$(`${this.enter}`).prepend(`
				<div class="catalog__item">
						<div class="catalog__img">
							<img class="img-fluid align-self-center" alt="Picture" title="Picture" src="../media/img/main-page-assets/chair-sample-${1 + r}.jpg">
						</div>
						<div class="catalog-desc catalog-desc__box">
							<div class="catalog-desc__title" data-carousel-init="company">${q}</div>
							<div class="catalog-desc__text" data-carousel-init="description">${w}</div>
							<div class="catalog-desc__price">
								<div class="catalog-desc__cost" data-carousel-init="price">${e}</div>
								<div class="catalog-desc__status" data-carousel-init="icanbuy"><span>#</span>в наличии</div>
							</div>
						</div>
					</div>
			`);
	}

	run() {
		const elem = document.querySelector(`${this.enter}`);
		// const output = document.querySelector(`${this.exit}`);
		if (elem) {
			this.constructor.info();

			const init = {
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				mode: 'cors',
				cache: 'default'
			};

			fetch(`./${this.server}`, init)
				.then((res) => {
					res.headers.get('Content-Type');
					return res.json();
				})
				// .then((data) => {
				// 	const select = document.querySelectorAll(`[${this.attr}]`);
				// 	const arr = [...select].map(i => (Object.assign(i.dataset)));
				// 	const newArr = arr.map(x => x.galleryInit);
				// 	newArr.map(i => (
				// 		document.querySelector(`[${this.attr}="${i}"]`).innerText = `${data.one[i] || 'null'}`));
				// })
				.then((myData) => {

					for (let i = 0; i < 6; i++) {
						this.createDiv(
							myData[i].company,
							myData[i].description,
							myData[i].price,
							i
						)
					};

				})
				.catch((error) => {
					console.log(`Ouch! Fetch error: \n ${error.message}`);
				});
		}
	}
};
